class MssspyWarning(UserWarning):
    """Warnings from `mssspy`."""


class ParseError(Exception):
    """Error in parsing of ms files."""
