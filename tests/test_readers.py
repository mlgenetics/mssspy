import pytest

from mssspy import MSReader


@pytest.mark.parametrize('reader', list(MSReader.registry.values()))
def test_readers(test_data, reader):
    """
    Test all readers against all data files in data/.

    Simply tests that each reader can read each sample in each file without
    crashing, and some other basic sanity checks.
    """

    for filename in test_data.glob('*.ms'):
        # Right now all the files in data/ are named in the format
        # {nsamp}_{nrep}.ms; might change this if other files in a different
        # format are added
        nsamp, nrep = [int(p) for p in filename.stem.split('_', 1)]
        reader_inst = reader(filename)
        for idx in range(nrep):
            samp = reader_inst.read_sample(idx)
            if len(samp.positions) == 0:
                # One shortcoming to this test is that it will not catch
                # non-empty samples that are mis-parsed.
                # This test is just meant to be a smoke test though; will add
                # some more specific test cases in other tests.
                assert samp.snps.shape == (0,)
            else:
                assert samp.snps.shape[0] == nsamp
                assert samp.snps.shape[1] == len(samp.positions)

        # Make sure reading beyond the last sample raises an IndexError
        with pytest.raises(IndexError):
            reader_inst.read_sample(nrep)
