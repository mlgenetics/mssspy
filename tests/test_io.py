import gzip
import io
import sys

import pytest

from mssspy import MSFile


@pytest.mark.parametrize('buftype', [io.StringIO, io.BytesIO])
def test_io_buffers(test_data, buftype):
    """Test reading from an `io.BytesIO`  or `io.StringIO` object."""

    # Read directly from the file
    samps1 = list(MSFile(test_data / '10_3.ms'))

    # Read data and wrap in String/BytesIO
    mode = 'r'
    if issubclass(buftype, io.BytesIO):
        mode += 'b'

    with open(test_data / '10_3.ms', mode) as fobj:
        buf = buftype(fobj.read())

    samps2 = list(MSFile(buf))
    assert samps1 == samps2


def test_read_gzip(test_data):
    """Test different cases for reading a gzip file."""

    # Trying to read a gzip file directly should fail (at least until/unless we
    # add automatic support for gzip files)
    with pytest.raises(IndexError):
        with MSFile(test_data / '10_3.ms.gz') as msf:
            msf[0]

    # Reading from a gzip.GzipFile should work
    with MSFile(gzip.open(test_data / '10_3.ms.gz')) as msf:
        all_samples_gzipped = list(msf)

    # Compare to the non-gzipped version of the same file
    with MSFile(test_data / '10_3.ms') as msf:
        all_samples = list(msf)

    assert all_samples_gzipped == all_samples


def test_nonseekable_file():
    """
    Should raise an exception when trying to read a non-seekable file.

    Later this should be supported, however; see:
    https://gitlab.com/mlgenetics/mssspy/-/issues/1
    """

    with pytest.raises(ValueError):
        MSFile(sys.stdin)
